# Simple demo of using PETSc vectors, matrices, and linear solvers in Python

## Suggested Conda environment
```sh
# For me
unset PETSC_DIR PETSC_ARCH PYTHONPATH
```
```sh
conda create -n "petsc4py-demo"  ipython mpich hdf5="*=mpi*" scipy petsc4py
conda activate "petsc4py-demo"
```
Be careful about HDF5 - serial version installed by default.

Vaclav, no need to always install PETSc from source 🤓

## Example 1: [`kspsolve.py`](kspsolve.py)
Assemble matrix and RHS using PETSc, solve using PETSc KSP.
```sh
./kspsolve.py
# OUTPUT:
# m, n: 3, 3
# RHS norm: 3.0
# iterations: 3 residual norm: 4.8074067159589095e-17
```
```sh
./kspsolve.py -verbose 1
./kspsolve.py -verbose 2
./kspsolve.py -verbose 3
```
```sh
./kspsolve.py -version
./kspsolve.py -help intro
./kspsolve.py -help
```
---
```sh
mpirun -n 2 ./kspsolve.py -verbose 3
# OUTPUT:
# m, n: 3, 3
# RHS norm: 3.0
# iterations: 3 residual norm: 8.326672684688674e-17
# x: <petsc4py.PETSc.Vec object at 0x1496a1270>
# 
# Vec Object: 2 MPI processes
#   type: mpi
# Process [0]
# 0.171875
# 0.21875
# 0.171875
# 0.21875
# 0.28125
# Process [1]
# 0.21875
# 0.171875
# 0.21875
# 0.171875
# 
# x (numpy):
# [0] : [0.171875 0.21875  0.171875 0.21875  0.28125 ]
# [1] : [0.21875  0.171875 0.21875  0.171875]
```
```sh
mpirun -n 3 ./kspsolve.py -m 2 -n 5 -verbose 3
```
---
```sh
mpirun -n 3 ./kspsolve.py -m 2 -n 5 -verbose 1 -ksp_converged_reason
# OUTPUT:
# m, n: 2, 5
# Linear solve converged due to CONVERGED_RTOL iterations 3
# RHS norm: 3.1622776601683795
# iterations: 3 residual norm: 3.5294082736068817e-16
```
```sh
mpirun -n 3 ./kspsolve.py -m 10 -n 100 -verbose 1 -ksp_converged_reason
# OUTPUT:
# m, n: 10, 100
# Linear solve did not converge due to DIVERGED_ITS iterations 100
# RHS norm: 31.622776601683793
# iterations: 100 residual norm: 0.06018197886847531
```
```sh
mpirun -n 3 ./kspsolve.py -m 10 -n 100 -verbose 1 -ksp_converged_reason -ksp_max_it 1000
# OUTPUT:
# m, n: 10, 100
# Linear solve converged due to CONVERGED_RTOL iterations 144
# RHS norm: 31.622776601683793
# iterations: 144 residual norm: 0.0002727213073933257
```
```sh
mpirun -n 3 ./kspsolve.py -m 10 -n 100 -verbose 1 -ksp_converged_reason -ksp_max_it 1000 \
  -ksp_rtol 1e-15 -ksp_atol 1e-8 -ksp_view -ksp_type gmres
# DIVERGED_ITS iterations 1000

mpirun -n 3 ./kspsolve.py -m 10 -n 100 -verbose 1 -ksp_converged_reason -ksp_max_it 1000 \
  -ksp_rtol 1e-15 -ksp_atol 1e-8 -ksp_view
# CONVERGED_ATOL iterations 203

mpirun -n 3 ./kspsolve.py -m 10 -n 100 -verbose 1 -ksp_converged_reason -ksp_max_it 1000 \
  -ksp_rtol 1e-15 -ksp_atol 1e-8 -ksp_view -pc_type bjacobi -sub_pc_type icc
# CONVERGED_ATOL iterations 31

mpirun -n 3 ./kspsolve.py -m 10 -n 100 -verbose 1 -ksp_converged_reason -ksp_max_it 1000 \
  -ksp_rtol 1e-15 -ksp_atol 1e-8 -ksp_view -pc_type bjacobi -sub_pc_type cholesky
# CONVERGED_ATOL iterations 23

mpirun -n 3 ./kspsolve.py -m 10 -n 100 -verbose 1 -ksp_converged_reason -ksp_max_it 1000 \
  -ksp_rtol 1e-15 -ksp_atol 1e-8 -ksp_view -pc_type bjacobi -sub_pc_type cholesky -ksp_type gmres
# CONVERGED_ATOL iterations 23

# NOTE: built-in direct solvers are sequential only...
mpirun -n 1 ./kspsolve.py -m 10 -n 100 -verbose 1 -ksp_converged_reason -ksp_max_it 1000 \
  -ksp_rtol 1e-15 -ksp_atol 1e-8 -ksp_view -pc_type cholesky
# CONVERGED_ATOL iterations 1
# iterations: 1 residual norm: 4.940115344840551e-12

mpirun -n 1 ./kspsolve.py -m 10 -n 100 -verbose 1 -ksp_converged_reason -ksp_view \
  -pc_type cholesky -ksp_type preonly
# CONVERGED_ITS iterations 1
# iterations: 1 residual norm: 0.0

# NOTE: ... but there are interfaces to external parallel direct solvers like MUMPS
#   MUMPS is automatically installed with petsc4py by Conda
mpirun -n 3 ./kspsolve.py -m 10 -n 100 -verbose 1 -ksp_converged_reason -ksp_view \
  -pc_type cholesky -ksp_type preonly -pc_factor_mat_solver_type mumps
# CONVERGED_ITS iterations 1
# iterations: 1 residual norm: 0.0
```

## Example 2: [`kspsolve_scipy.py`](kspsolve_scipy.py)
This example is like Example 1 except the matrix is assembled using SciPy.

This demonstrates extraction of local matrices from the serial SciPy matrix and
putting them into a distributed PETSc matrix.
```sh
mpirun -n 4 ./kspsolve_scipy.py -verbose 1 -n 10 -ksp_converged_reason -mat_view ::ascii_dense
```

TODO: it would be nice if there was a special function for that conversion directly in petsc4py.

## Example 3: [`kspsolve_qr.py`](kspsolve_qr.py)
This example loads a (rectangular) matrix from a HDF5 file
[[MAT-file v7.3](https://ch.mathworks.com/help/matlab/import_export/mat-file-versions.html)]
using PETSc Viewer API.
The non-square system is solved in the least-square sense using either built-in LSQR iterative solver,
or using SuiteSparseQR direct solver.
This is solver is quite interesting:
1. one of a few available and maintained sparse QR,
2. supports GPUs,
3. powers QR and `\` in MATLAB.

Note that SuiteSparseQR PETSc interface not yet available in Conda. I will use PETSc compiled from source here.

```sh
mpirun -n 4 ./kspsolve_qr.py  -filename ./small_rect.mat  -ksp_view -verbose -ksp_type lsqr -pc_type none

mpirun -n 4 ./kspsolve_qr.py  -filename ./rectangular_ultrasound_4889x841.mat  -ksp_view -verbose -ksp_type lsqr -pc_type none
```

```sh
# For me - petsc4py compiled from source with SuiteSparseQR
conda deactivate
. ~/petsc-6/env-arch-osx-dbg-conda-midi
```

```sh
mpirun -n 1 ./kspsolve_qr.py  -filename ./rectangular_ultrasound_4889x841.mat -ksp_type preonly -pc_type qr -pc_factor_mat_solver_type spqr -verbose -ksp_view
```

We can take a look at performance profile, details about version, configuration, command line options etc. by simply adding
```sh
-log_view
```
