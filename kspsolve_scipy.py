#!/usr/bin/env python3
import sys
import numpy as np
from scipy import sparse as sp
import petsc4py
petsc4py.init(sys.argv)
from petsc4py import PETSc


def assembleSciPySparseMat(n):
    data = np.ones((3, n))
    data[1, :] = -2.0 * np.ones(n)
    offsets = np.array([-1, 0, 1])
    return sp.dia_matrix((data, offsets), shape=(n, n))


# Assume all guys have the same original serial matrix S.
# Take its part S_loc which will form a local part of the parallel matrix A.
# Create parallel matrix A from the local parts S_loc. 
def scipy2petsc(comm, S, debug=False):
    S = S.tocsr()

    # Create just empty matrix to decide the ownership.
    # It would be nice to be able to do is in a simpler way 
    A = PETSc.Mat()
    A.create(comm)
    A.setSizes(S.shape)
    A.setUp()
    r = range(*A.getOwnershipRange())
    A.destroy()

    S_loc = S[r, :]  # get this process portion

    if debug:
        PETSc.Sys.syncPrint("[{}]: {}".format(comm.rank, r), comm=comm, flush=True)
        if comm.rank == 0:
            PETSc.Sys.syncPrint("S:\n{}\n".format(S.todense()), comm=comm)
        PETSc.Sys.syncPrint("[{}] S_loc:\n{}\n".format(comm.rank, S_loc.todense()), comm=comm, flush=True)

    # Create PETSc matrix from local CSR arrays
    A = PETSc.Mat().createAIJ(
        comm=comm,
        size=S.shape,
        csr=(S_loc.indptr, S_loc.indices, S_loc.data))
    return A


if __name__ == "__main__":
    comm = PETSc.COMM_WORLD
    rank = comm.rank
    OptDB = PETSc.Options()
    n = OptDB.getInt('n', 3)
    verbose = OptDB.getInt('verbose', 0)
    if verbose > 0: PETSc.Sys.Print("n: {}".format(n), comm=comm)

    # assemble the system matrix
    A = assembleSciPySparseMat(n)
    A = scipy2petsc(comm, A, verbose)

    # create vectors compatible with A so that A*x = b would work
    x, b = A.createVecs()

    # create linear solver and set the matrix as operator
    ksp = PETSc.KSP().create()
    ksp.setOperators(A)

    # set default settings and allow to override them from command line
    ksp.setType('cg')
    ksp.getPC().setType('none')
    ksp.max_it = 100
    ksp.rtol = 1e-5
    ksp.setNormType(PETSc.KSP.NormType.UNPRECONDITIONED)
    ksp.setFromOptions()

    # set x to all zeros, b to all ones
    x.set(0)
    b.set(1)

    # solve the system A*x = b
    ksp.solve(b, x)

    # demo of communicator-aware printing
    if verbose > 0:
        PETSc.Sys.Print("RHS norm: {}\niterations: {} residual norm: {}".format(
            b.norm(), ksp.its, ksp.norm), comm=comm)
    if verbose > 1:
        PETSc.Sys.Print("x: {}\n".format(x), comm=comm)
    if verbose > 2:
        x.view()   # note: command line option -vec_view can be used
        PETSc.Sys.Print(comm=comm)
        x_loc = x[range(*x.owner_range)]
        if rank == 0:
            PETSc.Sys.syncPrint("x (numpy):", comm=comm)
        PETSc.Sys.syncPrint("[{}] : {}".format(rank, x_loc), comm=comm, flush=True)
