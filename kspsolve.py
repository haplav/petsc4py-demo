#!/usr/bin/env python3
import sys
import numpy as np
import petsc4py
petsc4py.init(sys.argv)
from petsc4py import PETSc

def assembleMat(comm, m, n):
    # grid size and spacing
    hx = 1.0/(m-1)
    hy = 1.0/(n-1)

    # create sparse matrix
    A = PETSc.Mat()
    A.create(comm)
    A.setSizes([m*n, m*n])
    A.setType('aij') # sparse
    A.setFromOptions()
    A.setPreallocationNNZ(5)
    A.setUp()

    # precompute values for setting
    # diagonal and non-diagonal entries
    diagv = 2.0/hx**2 + 2.0/hy**2
    offdx = -1.0/hx**2
    offdy = -1.0/hy**2

    # loop over owned block of rows on this
    # processor and insert entry values
    Istart, Iend = A.getOwnershipRange()
    for I in range(Istart, Iend) :
        A[I,I] = diagv
        i = I//n    # map row number to
        j = I - i*n # grid coordinates
        if i> 0  : J = I-n; A[I,J] = offdx
        if i< m-1: J = I+n; A[I,J] = offdx
        if j> 0  : J = I-1; A[I,J] = offdy
        if j< n-1: J = I+1; A[I,J] = offdy

    # communicate off-processor values
    # and setup internal data structures
    # for performing parallel operations
    A.assemblyBegin()
    A.assemblyEnd()
    return A

if __name__ == "__main__":
    comm = PETSc.COMM_WORLD
    rank = comm.rank
    OptDB = PETSc.Options()
    m = OptDB.getInt('m', 3)
    n = OptDB.getInt('n', 3)
    verbose = OptDB.getInt('verbose', 0)
    if verbose > 0: PETSc.Sys.Print("m, n: {}, {}".format(m, n), comm=comm)

    # assemble the system matrix
    A = assembleMat(comm, m, n)

    # create vectors compatible with A so that A*x = b would work
    x, b = A.createVecs()

    # create linear solver and set the matrix as operator
    ksp = PETSc.KSP().create()
    ksp.setOperators(A)

    # set default settings and allow to override them from command line
    ksp.setType('cg')
    ksp.getPC().setType('none')
    ksp.max_it = 100
    ksp.rtol = 1e-5
    ksp.setNormType(PETSc.KSP.NormType.UNPRECONDITIONED)
    ksp.setFromOptions()

    # set x to all zeros, b to all ones
    x.set(0)
    b.set(1)

    # solve the system A*x = b
    ksp.solve(b, x)

    # demo of communicator-aware printing
    if verbose > 0:
        PETSc.Sys.Print("RHS norm: {}\niterations: {} residual norm: {}".format(
            b.norm(), ksp.its, ksp.norm), comm=comm)
    if verbose > 1:
        PETSc.Sys.Print("x: {}\n".format(x), comm=comm)
    if verbose > 2:
        x.view()   # note: command line option -vec_view can be used
        PETSc.Sys.Print(comm=comm)
        x_loc = x[range(*x.owner_range)]
        if rank == 0:
            PETSc.Sys.syncPrint("x (numpy):", comm=comm)
        PETSc.Sys.syncPrint("[{}] : {}".format(rank, x_loc), comm=comm, flush=True)
