#!/usr/bin/env python3
import sys
import numpy as np
from scipy import sparse as sp
import petsc4py
petsc4py.init(sys.argv)
from petsc4py import PETSc

if __name__ == "__main__":
    comm = PETSc.COMM_WORLD
    rank = comm.rank
    OptDB = PETSc.Options()
    filename = OptDB.getString('filename')

    verbose = OptDB.getBool('verbose', False)

    # create HDF5 loader with HDF5 (MAT-file 7.3) format
    # https://ch.mathworks.com/help/matlab/import_export/mat-file-versions.html
    loader = PETSc.Viewer().createHDF5(filename, comm=comm)
    loader.pushFormat(PETSc.Viewer.Format.HDF5_MAT)

    # load matrix
    A = PETSc.Mat().create(comm)
    A.setName('A')
    A.load(loader)

    # load RHS vector
    b = PETSc.Vec().create(comm)
    b.setName('b')
    b.load(loader)

    # create vectors compatible with A so that A*x = b would work
    x = A.createVecRight()
    x.setName('x')

    if verbose:
        PETSc.Sys.Print("global sizes:", comm=comm)
        PETSc.Sys.Print("{}: {}  {}: {}  {}: {}".format(
            A.name, A.size, b.name, b.size, x.name, x.size), comm=comm)
        if comm.rank == 0:
            PETSc.Sys.syncPrint("local sizes:", comm=comm)
        PETSc.Sys.syncPrint("[{}] {}: {}  {}: {}  {}: {}".format(
            comm.rank, A.name, A.local_size, b.name, b.size, x.name, x.size), comm=comm, flush=True)

    # create linear solver and set the matrix as operator
    ksp = PETSc.KSP().create()
    ksp.setOperators(A)

    # set default settings and allow to override them from command line
    ksp.setType('lsqr')
    ksp.getPC().setType('none')
    ksp.max_it = 100
    ksp.rtol = 1e-5
    ksp.setNormType(PETSc.KSP.NormType.UNPRECONDITIONED)
    ksp.setFromOptions()

    # set x to all zeros, b to all ones
    x.set(0)
    b.set(1)

    # solve the system A*x = b
    ksp.solve(b, x)

    if verbose:
        # demo of communicator-aware printing
        PETSc.Sys.Print("RHS norm: {}\niterations: {} residual norm: {}".format(
            b.norm(), ksp.its, ksp.norm), comm=comm)

        # compute residual on my own
        y = A.createVecLeft()
        A.mult(x,y)    # y = A*x
        y.axpy(-1.0,b) # y = y - b
        PETSc.Sys.Print("my residual norm ||A*x - b||: {}".format(y.norm()), comm=comm)
